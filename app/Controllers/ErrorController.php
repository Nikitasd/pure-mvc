<?php

namespace App\Controllers;

use Core\Controller;

class ErrorController extends Controller
{
    /*
     * Error page 404
     *
     * */
    public function notFoundAction()
    {
        http_response_code(404);

        $this->view('errors/404');
    }
}