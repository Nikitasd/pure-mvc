<?php

namespace App\Controllers;

use Core\Controller;
use App\Classes;
use App\Models;

class LoginController extends Controller
{
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    public function indexAction()
    {
        // Check that the user is unauthenticated.
        Classes\Auth::checkUnauthenticated();

        $this->view('auth/login');
    }

    /**
     * Processes a login request
     *
     * @return void
     */
    public function loginAction()
    {
        // Check that the user is unauthenticated.
        Classes\Auth::checkUnauthenticated();

        if (Models\UserLogin::login()) {
            Classes\Redirect::to($this->redirectTo);
        }

        Classes\Redirect::to('/login');
    }

    /**
     * Processes a logout request
     *
     * @return void
     */
    public function logoutAction()
    {
        if (isset($_POST["logout"])) {
            // Check that the user is authenticated.
            Classes\Auth::checkAuthenticated();

            if (Models\UserLogin::logout()) {
                Classes\Redirect::to("login");
            }
        }
        Classes\Redirect::to($this->redirectTo);
    }
}