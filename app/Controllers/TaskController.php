<?php

namespace App\Controllers;

use Core\Controller;
use App\Models;
use App\Classes;

class TaskController extends Controller
{
    private $task;

    public function __construct()
    {
        $this->task = new Models\Task();
    }

    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {
        $paginate = new Models\Paginate('tasks',3, "justify-content-center");

        $tasks = $paginate->getPaginate();

        $this->view('tasks/index', [
            'tasks' => $tasks,
        ]);
    }

    public function addAction()
    {
        $this->view('tasks/add');
    }


    /*
     * Save the task to the database.
     * Post request.
     *
     * */
    public function createAction()
    {
        if (isset($_POST["submit_add_task"])) {

            $request = [
                'username' => Classes\Input::post("username"),
                'email' => Classes\Input::post("email"),
                'comment' => Classes\Input::post("comment"),
                'created_at' => date("Y-m-d H:i:s")
            ];

            $this->task->save($request);

            Classes\Redirect::to($_ENV['APP_URL']);
        } else {
            throw new \Exception('Something went wrong');
        }
    }

    /*
     * Show the task page
     *
     * Access only for admin
     *
     * @return void
     * */
    public function editAction()
    {
        // Check that the user is unauthenticated.
        Classes\Auth::checkAuthenticated();

        $task_id = Classes\Input::get('id');

        // Check that correct uri
        $this->task->checkUri($task_id);

        $this->view('tasks/edit', [
            'task' => $this->task->getTaskById($task_id)
        ]);
    }

    /*
     * Update the task to the database.
     * Post request.
     *
     * Access only for admin
     *
     * @return void
     * */
    public function updateAction()
    {
        Classes\Auth::checkAuthenticated();

        if (isset($_POST["submit_edit_task"])) {

            $request = [
                'id' => Classes\Input::post("id"),
                'username' => Classes\Input::post("username"),
                'email' => Classes\Input::post("email"),
                'comment' => Classes\Input::post("comment"),
                'status' => Classes\Input::post('status') ? true : false,
                'created_at' => date("Y-m-d H:i:s")
            ];

            $this->task->update($request);

            Classes\Redirect::to($_ENV['APP_URL']);
        } else {
            throw new \Exception('Something went wrong');
        }
    }
}