<?php

namespace App\Models;

use Core\Model as ActiveRecord;

class Task extends ActiveRecord
{
    /**
     * Get all the tasks
     *
     * @return array
     */
    public function getTaskById($id)
    {
        return $this->getAll("WHERE `id` = {$id}")[0];
    }

    /**
     * Check ueser uri
     *
     * @return array
     */
    public function checkUri($id)
    {
        $uri = true;
        if(empty($id)) {
            $uri = false;
        }

        return $uri;
    }


}