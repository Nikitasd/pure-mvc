<?php

namespace App\Models;

use Core\Model as ActiveRecord;
use App\Classes;


class UserLogin extends ActiveRecord
{

    public static function login()
    {

        $username = Classes\Input::post("username");
        $userData = User::getInstance($username);
        $message_error = "";

        if (!$userData) {
            $message_error = "Wrong login or password";
        }

        // Check if the provided password fits the hashed password
        $password = Classes\Input::post("password");


        if (hash("sha256", $password) !== $userData["password"]) {
            $message_error = "Wrong login or password";
        }

        Classes\Session::init();

        if(!empty($message_error)) {
            Classes\Session::set_value("message_error", $message_error);
            return false;
        }

        Classes\Session::set_value('user', $userData['id']);

        return true;
    }

    /**
     * Log out user
     *
     * @return boolean
     */
    public static function logout()
    {
        // Destroy all data registered to the session.
        Classes\Session::remove_all();

        return true;
    }
}