<?php

namespace App\Models;

use Core\Model as ActiveRecord;

class User extends ActiveRecord
{
    /*
     * Retrieves and stores a specified record from the database
     *
     * @param string $user
     * */
    public static function getInstance($username)
    {
        if ($userModel = parent::query('SELECT * FROM `users` WHERE `username` = "'. $username.'"')) {
            return array_shift($userModel);
        }
        return null;
    }
}