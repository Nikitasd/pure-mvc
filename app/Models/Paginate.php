<?php

namespace App\Models;

use Core\Model as ActiveRecord;
use App\Classes;

class Paginate extends ActiveRecord
{
    /**
     * number of items per page.
     *
     * @var numeric
     */
    private $perPage;

    private $customCSS;

    private $paginate;

    private $tableName;

    public function __construct($tableName, $perPage = 12, $customCSS = null)
    {
        $this->tableName = $tableName;

        $this->perPage = $perPage;

        $this->customCSS = $customCSS;

        $this->paginate = new Classes\Paginate($this->perPage,'p', $this->customCSS);
    }

    /**
     * Get pagination data
     *
     * @return array
     */
    public function getPaginate()
    {
        $this->paginate->set_total( $this->getCountEntries() );

        $data['records'] = $this->getPaginationRecords();

        $data['page_links'] = $this->paginate->page_links();

        return $data;
    }

    /**
     * Get pagination records
     *
     * @return array
     */
    private function getPaginationRecords()
    {
        $totalDisplay = $this->paginate->current_page() * $this->perPage - $this->perPage;

        $paramSort =  Classes\Input::get('sort');

        if (!empty($paramSort)) {

            $paramOrderBy = Classes\Input::get('orderBy');

            if(!empty($orderBy))
                $paramOrderBy = "ASC";

            $records = $this->sort($paramSort, $paramOrderBy, $totalDisplay, $this->perPage);
        } else {
            $records = $this->sort('username', 'DESC', $totalDisplay, $this->perPage);
        }
        return $records;
    }

    /**
     * Sort pagination data
     *
     * @param string|integer $param, string $orderBy, integer $start, integer $num
     *
     * @return array
     */
    private function sort($param, $orderBy = 'ASC', $start = 1, $num = 12)
    {
        return $this->query("SELECT * FROM {$this->tableName} ORDER BY {$param} {$orderBy} LIMIT {$start} , {$num} ");
    }

    /**
     * Get entries count
     *
     * @return integer
     */
    public function getCountEntries()
    {
        $count = $this->query("SELECT COUNT(*) FROM {$this->tableName} ");

        return $count[0]["COUNT(*)"];

    }
}