<?php

namespace App\Classes;

class Session
{
    /**
     * Sets a specific value to a specific key of the session.
     *
     * @return string
     */
    public static function set_value($var, $value)
    {
        $_SESSION[$var] = $value;
    }

    /**
     * Get session value
     *
     * @return string
     */
    public static function get_value($var)
    {
        $session_var = null;
        if (self::exists($var)) {
            $session_var = $_SESSION[$var];
        }
        return $session_var;
    }

    /**
     * Deletes the session.
     *
     * @return void
     */
    public static function remove_all()
    {
        session_destroy();
    }

    /**
     * Deletes the value of a specific key of the session.
     *
     * @return boolean
     */
    public static function unset_value($var)
    {
        if (self::exists($var)) {
            unset($_SESSION[$var]);
            return true;
        }
        return false;
    }

    /**
     * Checks if a specific key of a session exists
     * @param string $key
     *
     * @return boolean
     */
    public static function exists($key)
    {
        return(!empty($_SESSION[$key]));
    }
    /**
     * Starts the session.
     *
     * @return void
     */
    public static function init() {
        // If no session exist, start the session.
        if (session_id() == "") {
            session_start();
        }
    }
}