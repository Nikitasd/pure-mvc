<?php

namespace App\Classes;

use App\Classes\Session;

class Auth
{
    /**
     * Checks to see if the user is authenticated
     *
     * @param string $redirect
     */
    public static function checkAuthenticated($redirect = "login") {
        Session::init();
        if (!Session::exists('user')) {
            Session::unset_value('user');
            Redirect::to($_ENV["APP_URL"] . $redirect);
        }
    }

    /**
     * Checks to see if the user is unauthenticated
     *
     * @param string $redirect
     */
    public static function checkUnauthenticated($redirect = "/") {
        Session::init();
        if (Session::exists('user')) {
            Redirect::to($_ENV["APP_URL"] . $redirect);
        }
    }


}