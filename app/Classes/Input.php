<?php

namespace App\Classes;

class Input
{
    /**
     * Returns the value of a specific key of the GET super-global
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public static function get($key, $default = "") {
        return(!empty($_GET[$key]) ? $_GET[$key] : $default);
    }

    /**
     * Returns the value of a specific key of the POST super-global
     *
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public static function post($key, $default = "") {
        return(!empty($_POST[$key]) ? $_POST[$key] : $default);
    }
}