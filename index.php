<?php

require_once __DIR__. '/vendor/autoload.php';

/**
 * Error and Exception handling
 */
ini_set('display_errors', '0');


/*
 * Load .env
 */
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();
/**
 * Routing
 */
$router = new Core\Router();

// Add the routes
$router->add('/', ['controller' => 'TaskController', 'action' => 'index'], ['sort=.*', 'orderBy=(asc|desc|)', 'p=[0-9]+']);
$router->add('/add', ['controller' => 'TaskController', 'action' => 'add']);
$router->add('/create', ['controller' => 'TaskController', 'action' => 'create']);
$router->add('/task/edit', ['controller' => 'TaskController', 'action' => 'edit'], ["id=.*" ]);
$router->add('/task/update', ['controller' => 'TaskController', 'action' => 'update']);

$router->add('/login', ['controller' => 'LoginController', 'action' => 'index']);
$router->add('/login-execute', ['controller' => 'LoginController', 'action' => 'login']);
$router->add('/logout', ['controller' => 'LoginController', 'action' => 'logout']);
$router->add('/404', ['controller' => 'ErrorController', 'action' => 'notFound']);


$router->dispatch($_SERVER['REQUEST_URI']);
