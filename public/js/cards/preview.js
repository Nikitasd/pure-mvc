$("#preview").on("click", function() {
    var card_title = $("#username").val() + " | " + $("#email").val(),
        card_comment = $("#comment").val(),
        card_status = $("#status-task").val() == 1 ? "Confirmed" : "Denied";

    $("#tasks .card-title").text(card_title);
    $("#tasks .card-comment").text(card_comment);
    $("#tasks .card-status").text(card_status);
});

$("form").bind('ajax:complete', function() {
    $(this).find("button[type='submit']").prop('disabled',true);
})