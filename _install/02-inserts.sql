insert into `users`
  (`username`, `password`, `email`, `salt`)
values
  ('admin', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 'admin@gmail.com', 'E1F53135E559C253');

insert into tasks
  (username, email, comment, status)
values
  ('test', 'test@test.com', 'test job', FALSE ),
  ('new-test', 'lorem-ipsum@gmail.com', 'Lorem ipsum dollor 4', FALSE);