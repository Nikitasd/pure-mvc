# Pure php mvc

## Installation
* `git clone https://gitlab.com/Nikitasd/pure-mvc`
* `composer install`
* `cp .env.example .env`
* `php -S localhost:8000`

### Install Database
```bash
cat _install/01-structure.sql | mysql --user=root --password=password myApp
cat _install/02-inserts.sql | mysql --user=root --password=password myApp
```

## [Demo](http://bj-app.zzz.com.ua/)