<?php

namespace Core;

use PDO;

final class Connection
{

    private function __construct()
    {
    }

    /**
     * Get the PDO database connection
     *
     * @return mixed
     */
    public static function getDb()
    {
        static $db = null;

        if ($db === null) {

            $host = $_ENV['DB_HOST'];
            $user = $_ENV['DB_USERNAME'];
            $pass = $_ENV['DB_PASSWORD'];
            $dbDatabase = $_ENV['DB_DATABASE'];

            $conn = 'mysql:host=' . $host . ';dbname=' . $dbDatabase . ';charset=utf8';
            $db = new PDO($conn, $user, $pass);

            // Throw an Exception when an error occurs
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }

        return $db;
    }
}
