<?php

namespace Core;

class Controller
{

    protected $route_params = [];


    public function __construct($route_params)
    {
        $this->route_params = $route_params;
    }


    public function view($template, $args = [])
    {
        // Create a new view and display the parsed contents
        $view = new View($template, $args);

        echo $view;
    }

    /**
     * Load a model
     *
     * @param string $model The name of the model to load
     *git
     * @return object
     */

    public function __call($name, $args)
    {
        $method = $name . 'Action';

        if (method_exists($this, $method)) {
            if ($this->before() !== false) {
                call_user_func_array([$this, $method], $args);
                $this->after();
            }
        } else {
            throw new \Exception("Method $method not found in controller " . get_class($this));
        }
    }

    /**
     * @return void
     */
    protected function before()
    {
    }

    /**
     * @return void
     */
    protected function after()
    {
    }
}
