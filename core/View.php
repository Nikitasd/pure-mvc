<?php

namespace Core;

use \Twig\Loader\FilesystemLoader;
use \Twig\Environment;
use App\Classes;

class View
{

    /**
     * View file
     *
     * @var string
     */
    private $template;

    /**
     * The data of the view
     *
     * @var array
     */
    private $args;

    /**
     * The actual templater
     *
     * @var Twig_Environment
     */
    private $twig;

    /**
     * Initialize a new View
     *
     * @param $template
     */


    public function __construct($template, $args = [])
    {
        $this->template = $template;
        $this->args = $args;

        $twigLoader = new FilesystemLoader(dirname(__DIR__) . '/app/Views');
        $this->twig = new Environment($twigLoader);

    }

    /**
     * Return the parsed view
     *
     * @return string
     */
    public function __toString()
    {
        return $this->parseView();
    }

    /**
     * Parse the view into a string using Twig
     *
     * @return string
     */
    public function parseView()
    {
        $twigVariables = $this->getGlobalVariables();

        if(!empty($twigVariables))
            $this->args['globals'] = $twigVariables;

        try {
            $template = $this->twig->load($this->template . ".html");
            return $this->twig->render($template, $this->args);
        } catch (\Twig\Error\LoaderError $e) {
            return $this->getErrorMessage('template loading', $e->getMessage());
        } catch (\Twig\Sandbox\SecurityError $e) {
            return $this->getErrorMessage('unallowed tag', $e->getMessage());
        } catch (\Twig\Error\SyntaxError $e){
            return $this->getErrorMessage('template syntax', $e->getMessage());
        } catch (\Twig\Error\Error $e){
            return $this->getErrorMessage('base exception', $e->getMessage());
        }
    }

    private function getErrorMessage($errorType, $errorMessage)
    {
        return sprintf("A %s error occured: %s", $errorType, $errorMessage);
    }

    /**
     * Set global twig variables
     *
     * @return array
     */
    public function getGlobalVariables()
    {
        Classes\Session::init();

        return [
            'is_auth_user' => Classes\Session::exists('user'),
            'message_error' => Classes\Session::get_value("message_error")
        ];
    }

}
