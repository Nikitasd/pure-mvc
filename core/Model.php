<?php

namespace Core;

use PDO;

abstract class Model
{

    public function query($query)
    {
        if($conn = Connection::getDb())
        {
            $objects = [];

            $result = $conn->query($query);

            if($result)
            {
                $objects = $result->fetchAll(PDO::FETCH_ASSOC);
            }

            return $objects;
        } else {
            throw new Exception('You need an active transaction to perform this operation');
        }
    }

    public function getAll($options = null)
    {
        $query = "SELECT * FROM {$this->getEntity()}";

        ($options !== null) ? $query .= " {$options}": $query;

        return $this->query($query);
    }

    private function getEntity()
    {
        $currentClassName = substr(strrchr(get_class($this), "\\"), 1);

        //the table matches to the class name.
       return strtolower($currentClassName . "s");
    }

    /*
     *
     * Save the model to the database.
     *
     *  @param array $request, boolean $slash
     *
     * @return @object
     *
     * @throws Exception
     * */
    public function save(array $request, $slash = true)
    {
        if(empty($request))
            return false;

        foreach($request as $request_key => $request_value) {
            if($slash) { $res[$request_key] = is_null($request_value) ? "NULL" : "'" . addslashes($request_value) . "'"; }
            else { $res[$request_key] = is_null($request_value) ? "NULL" : "'{$request_value}'"; }
        }

        $request_keys = implode(', ', array_keys($res));
        $request_values = implode(', ', array_values($res));

        $query = "INSERT INTO {$this->getEntity()} ({$request_keys}) VALUES ({$request_values})";

        if($conn = Connection::getDb()) {

            $result = $conn->exec($query);
            return $result;
        } else {
            throw new Exception('You need an active transaction to perform this operation');
        }

    }

    public function update($request, $options = NULL)
    {
        foreach($request as $column => $value)
        {
            $value = is_string($value) ? "'" . addslashes($value) . "'" : $value;
            $data[] = "{$column} = {$value}";
        }


        (!$options) ? $options = 'WHERE id = ' . $request['id'] : $options = $options;
        $query = "UPDATE {$this->getEntity()} SET " . implode(',', $data) . " {$options}";


        if($conn = Connection::getDb()) {

            $result = $conn->exec($query);
            return $result;
        } else {
            throw new Exception('You need an active transaction to perform this operation');
        }
    }
}